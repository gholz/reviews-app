## Synopsis

This is an example project that loads and add's reviews from the web, this application uses the MVVM architecture and the following technologies:
Realm.io, RxJava, Databinding, Dagger 2 and Retrofit.

## Installation

- Clone the repository.
- Import in Android Studio or IntelliJ.

## Contributors

Guilherme Holz - guilhermemh@gmail.com

## License

This software is under the MIT license.