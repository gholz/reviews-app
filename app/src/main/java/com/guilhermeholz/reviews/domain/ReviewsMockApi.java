package com.guilhermeholz.reviews.domain;

import com.guilhermeholz.reviews.model.Review;
import com.guilhermeholz.reviews.model.AddReviewResponse;

import retrofit2.http.Body;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by gholz on 6/3/16.
 */
public interface ReviewsMockApi {
    @POST("/reviews/add")
    Observable<AddReviewResponse> addReview(@Body Review review);
}
