package com.guilhermeholz.reviews.domain;

import com.guilhermeholz.reviews.model.ReviewsResponse;

import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by gholz on 6/1/16.
 */
public interface ReviewsApi {
    @GET("/{city}/{tour}/reviews.json")
    Observable<ReviewsResponse> getReviews(@Path("city") String cityId,
                                           @Path("tour") String tourId,
                                           @Query("count") int count,
                                           @Query("page") int page,
                                           @Query("rating") int rating,
                                           @Query("sortBy") String sortMethod,
                                           @Query("direction") String direction);
}
