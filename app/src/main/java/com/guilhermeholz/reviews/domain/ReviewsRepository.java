package com.guilhermeholz.reviews.domain;

import android.content.Context;
import android.content.SharedPreferences;

import com.guilhermeholz.reviews.ReviewsApp;
import com.guilhermeholz.reviews.model.AddReviewResponse;
import com.guilhermeholz.reviews.model.Review;
import com.guilhermeholz.reviews.util.NetworkUtils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * This class represents an abstraction layer
 * between application logic and the data sources.
 */
public class ReviewsRepository {

    public static final String CITY_ID = "berlin-l17";
    public static final String TOUR_ID = "tempelhof-2-hour-airport-history-tour-berlin-airlift-more-t23776";
    public static final String SORT_METHOD = "date_of_review";
    public static final String DIRECTION = "DESC";
    public static final String PREFERENCE_LAST_ID = "last_id";
    public static final int DEFAULT_COUNT = 20;

    @Inject
    protected Realm realm;

    @Inject
    protected ReviewsApi api;

    @Inject
    protected ReviewsMockApi mockApi;

    @Inject
    protected SharedPreferences preferences;

    private Context context;
    private long lastId;

    public ReviewsRepository(Context context) {
        ReviewsApp.getApplication(context).getComponent().inject(this);
        lastId = preferences.getLong(PREFERENCE_LAST_ID, 0);
        this.context = context;
    }

    public Observable<List<Review>> getReviews() {
        return getReviews(DEFAULT_COUNT);
    }

    public Observable<List<Review>> getAllReviews() {
        return getReviews(0);
    }

    private Observable<List<Review>> getReviews(int count) {

        if (NetworkUtils.hasConnectivity(context)) {
            return api.getReviews(CITY_ID, TOUR_ID, count, 0, 0, SORT_METHOD, DIRECTION)
                    .subscribeOn(Schedulers.io())
                    .map(response -> response.reviews)
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnNext(this::persist)
                    .flatMap(re -> realm.where(Review.class)
                            .findAllSortedAsync("id", Sort.DESCENDING)
                            .asObservable()
                            .map(results -> toReviewsList(results, DEFAULT_COUNT)));
        } else {
            return realm.where(Review.class)
                    .findAllSortedAsync("id", Sort.DESCENDING)
                    .asObservable()
                    .map(results -> toReviewsList(results, count));
        }
    }

    private List<Review> toReviewsList(RealmResults<Review> results, int size) {

        ArrayList<Review> reviews = new ArrayList<>();

        int length = results.size() < size || size == 0 ? results.size() : size;
        for (int i = 0; i < length; i++) {
            reviews.add(results.get(i));
        }

        return reviews;
    }

    private void persist(List<Review> reviews) {
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(reviews);
        realm.commitTransaction();
    }

    public Observable<AddReviewResponse> addReview(Review review) {

        if (lastId == 0) {
            Review first = realm.where(Review.class).findFirst();
            if (first != null) {
                lastId = first.id;
            } else {
                lastId = 400000;
            }
        }

        review.id = lastId + 1;

        if (NetworkUtils.hasConnectivity(context)) {
            return mockApi.addReview(review)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnNext(response -> {
                        review.id = lastId = response.resultId;
                        saveReview(review);
                    });
        } else {

            lastId = review.id;

            review.pendingSync = true;
            saveReview(review);

            return Observable.just(new AddReviewResponse(true, "", lastId));
        }
    }

    private void saveReview(Review review) {

        preferences.edit()
                .putLong(PREFERENCE_LAST_ID, lastId)
                .apply();

        realm.beginTransaction();
        realm.copyToRealm(review);
        realm.commitTransaction();
    }
}
