package com.guilhermeholz.reviews.ui.activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.guilhermeholz.reviews.R;
import com.guilhermeholz.reviews.databinding.ReviewsListBinding;
import com.guilhermeholz.reviews.model.Review;
import com.guilhermeholz.reviews.ui.adapters.ReviewsAdapter;
import com.guilhermeholz.reviews.view.ReviewsListView;
import com.guilhermeholz.reviews.viewmodel.ReviewsListViewModel;

import java.util.List;

public class ReviewsListActivity extends AppCompatActivity implements ReviewsListView {

    public static final int REQUEST_CODE_ADD_REVIEW = 6000;

    private ReviewsAdapter adapter;
    private ReviewsListBinding binding;
    private ReviewsListViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        adapter = new ReviewsAdapter(this);

        binding = DataBindingUtil.setContentView(
                this, R.layout.activity_reviews_list);

        viewModel = new ReviewsListViewModel(this, this);

        binding.reviewsList.setLayoutManager(new LinearLayoutManager(this));
        binding.reviewsList.setAdapter(adapter);
        binding.swipe.setOnRefreshListener(() ->
                viewModel.loadReviews(adapter.getItemCount() > 20));
        binding.setViewModel(viewModel);

        viewModel.loadReviews(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_reviews_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.action_add_review) {
            Intent intent = new Intent(this, AddReviewActivity.class);
            startActivityForResult(intent, REQUEST_CODE_ADD_REVIEW);
        }

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_ADD_REVIEW && resultCode == RESULT_OK) {
            viewModel.loadReviews(false);
        }
    }

    @Override
    public void displayReviews(List<Review> reviews) {
        adapter.setReviews(reviews);
        binding.swipe.setRefreshing(false);
    }

    @Override
    public void displayError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
        binding.swipe.setRefreshing(false);
    }
}
