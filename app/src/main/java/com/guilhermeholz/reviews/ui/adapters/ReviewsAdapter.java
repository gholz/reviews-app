package com.guilhermeholz.reviews.ui.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.guilhermeholz.reviews.R;
import com.guilhermeholz.reviews.databinding.ReviewItemBinding;
import com.guilhermeholz.reviews.model.Review;
import com.guilhermeholz.reviews.viewmodel.ReviewItemViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gholz on 6/1/16.
 */
public class ReviewsAdapter extends RecyclerView.Adapter<ReviewsAdapter.ReviewItemViewHolder> {

    private List<Review> reviews;
    private LayoutInflater inflater;

    public ReviewsAdapter(Context context) {
        reviews = new ArrayList<>();
        inflater = LayoutInflater.from(context);
    }

    @Override
    public ReviewItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        ReviewItemBinding binding = DataBindingUtil.inflate(inflater,
                R.layout.item_review, parent, false);

        return new ReviewItemViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ReviewItemViewHolder holder, int position) {

        ReviewItemViewModel viewModel = holder.binding.getViewModel();
        if (viewModel == null) {
            viewModel = new ReviewItemViewModel();
            holder.binding.setViewModel(viewModel);
        }

        viewModel.display(reviews.get(position));
    }

    @Override
    public int getItemCount() {
        return reviews.size();
    }

    public void setReviews(List<Review> reviews) {
        this.reviews.clear();
        this.reviews.addAll(reviews);
        notifyDataSetChanged();
    }

    public class ReviewItemViewHolder extends RecyclerView.ViewHolder {

        public ReviewItemBinding binding;

        public ReviewItemViewHolder(ReviewItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
