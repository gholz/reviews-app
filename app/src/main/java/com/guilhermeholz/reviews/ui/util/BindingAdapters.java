package com.guilhermeholz.reviews.ui.util;

import android.databinding.BindingAdapter;
import android.view.View;
import android.widget.RatingBar;

/**
 * Created by gholz on 6/2/16.
 */
public class BindingAdapters {

    @BindingAdapter("visible")
    public static void setVisible(View view, boolean visible) {
        view.setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    @BindingAdapter("onRatingBarChangeListener")
    public static void setOnRatingListener(RatingBar view, RatingBar.OnRatingBarChangeListener listener) {
        view.setOnRatingBarChangeListener(listener);
    }
}
