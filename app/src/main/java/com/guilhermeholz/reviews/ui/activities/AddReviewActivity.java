package com.guilhermeholz.reviews.ui.activities;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.guilhermeholz.reviews.R;
import com.guilhermeholz.reviews.databinding.AddReviewBinding;
import com.guilhermeholz.reviews.view.AddReviewView;
import com.guilhermeholz.reviews.viewmodel.AddReviewViewModel;

/**
 * Created by gholz on 6/2/16.
 */
public class AddReviewActivity extends AppCompatActivity implements AddReviewView {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AddReviewBinding binding = DataBindingUtil.setContentView(this,
                R.layout.activity_add_review);
        binding.setViewModel(new AddReviewViewModel(this, this));
    }

    @Override
    public void onSuccess() {
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void displayError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }
}
