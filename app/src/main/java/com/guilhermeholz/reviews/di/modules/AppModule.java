package com.guilhermeholz.reviews.di.modules;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.guilhermeholz.reviews.ReviewsApp;
import com.guilhermeholz.reviews.domain.ReviewsApi;
import com.guilhermeholz.reviews.domain.ReviewsMockApi;
import com.guilhermeholz.reviews.domain.ReviewsRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by gholz on 6/2/16.
 */
@Module
public class AppModule {

    private ReviewsApp application;

    public AppModule(ReviewsApp application) {
        this.application = application;
    }

    @Provides
    @Singleton
    public ReviewsRepository provideRepository(Realm realm, ReviewsApi api, ReviewsMockApi mockApi) {
        return new ReviewsRepository(application);
    }

    @Provides
    @Singleton
    public RealmConfiguration provideConfiguration() {
        return new RealmConfiguration.Builder(application)
                .deleteRealmIfMigrationNeeded()
                .build();
    }

    @Provides
    @Singleton
    public Realm provideRealm(RealmConfiguration configuration) {
        return Realm.getInstance(configuration);
    }

    @Provides
    @Singleton
    public SharedPreferences providePreferences() {
        return PreferenceManager.getDefaultSharedPreferences(application);
    }
}
