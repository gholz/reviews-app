package com.guilhermeholz.reviews.di.components;

import com.guilhermeholz.reviews.di.modules.AppModule;
import com.guilhermeholz.reviews.di.modules.NetworkModule;
import com.guilhermeholz.reviews.domain.ReviewsRepository;
import com.guilhermeholz.reviews.viewmodel.AddReviewViewModel;
import com.guilhermeholz.reviews.viewmodel.ReviewsListViewModel;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by gholz on 6/2/16.
 */
@Singleton
@Component(modules = {AppModule.class, NetworkModule.class})
public interface AppComponent {
    void inject(ReviewsListViewModel viewModel);
    void inject(AddReviewViewModel viewModel);
    void inject(ReviewsRepository repository);
}
