package com.guilhermeholz.reviews.di.modules;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.guilhermeholz.reviews.domain.ReviewsApi;
import com.guilhermeholz.reviews.domain.ReviewsMockApi;
import com.guilhermeholz.reviews.model.AddReviewResponse;
import com.guilhermeholz.reviews.model.Review;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import rx.Observable;

/**
 * Created by gholz on 6/2/16.
 */
@Module
public class NetworkModule {

    @Provides @Singleton
    public ReviewsMockApi provideMockApi() {
        return review -> {
            AddReviewResponse response = new AddReviewResponse(true, "Review added", review.id);
            return Observable.just(response);
        };
    }

    @Provides @Singleton
    public ReviewsApi provideApi(GsonConverterFactory converterFactory,
                                 RxJavaCallAdapterFactory callAdapterFactory) {
        return new Retrofit.Builder()
                .baseUrl("https://www.getyourguide.com")
                .addConverterFactory(converterFactory)
                .addCallAdapterFactory(callAdapterFactory)
                .build()
                .create(ReviewsApi.class);
    }

    @Provides @Singleton
    public RxJavaCallAdapterFactory provideRxJavaCallAdapterFactory() {
        return RxJavaCallAdapterFactory.create();
    }

    @Provides @Singleton
    public GsonConverterFactory provideGsonConverter(Gson gson) {
        return GsonConverterFactory.create(gson);
    }

    @Provides @Singleton
    public Gson provideGson() {
        return new GsonBuilder().create();
    }
}
