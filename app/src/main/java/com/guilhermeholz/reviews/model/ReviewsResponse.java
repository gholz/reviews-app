package com.guilhermeholz.reviews.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by gholz on 6/1/16.
 */
public class ReviewsResponse {

    @SerializedName("data")
    public List<Review> reviews;

    @SerializedName("total_reviews")
    public int total;
}
