package com.guilhermeholz.reviews.model;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by gholz on 6/1/16.
 */
public class Review extends RealmObject {

    @PrimaryKey
    @SerializedName("review_id")
    public long id;

    @SerializedName("rating")
    public float rating;

    @SerializedName("title")
    public String title;

    @SerializedName("message")
    public String message;

    @SerializedName("author")
    public String author;

    @SerializedName("date")
    public String date;

    @SerializedName("languageCode")
    public String languageCode;

    public boolean pendingSync;
}
