package com.guilhermeholz.reviews.model;

/**
 * Created by gholz on 6/2/16.
 */
public class AddReviewResponse {

    public boolean success;
    public String message;
    public long resultId;

    public AddReviewResponse(boolean success, String message, long resultId) {
        this.success = success;
        this.message = message;
        this.resultId = resultId;
    }
}
