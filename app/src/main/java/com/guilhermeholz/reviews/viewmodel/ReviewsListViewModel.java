package com.guilhermeholz.reviews.viewmodel;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.ObservableBoolean;
import android.util.Log;
import android.view.View;

import com.guilhermeholz.reviews.ReviewsApp;
import com.guilhermeholz.reviews.domain.ReviewsRepository;
import com.guilhermeholz.reviews.model.Review;
import com.guilhermeholz.reviews.view.ReviewsListView;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by gholz on 6/2/16.
 */
public class ReviewsListViewModel extends BaseObservable {

    private static final String LOG_TAG = ReviewsListViewModel.class.getSimpleName();

    public ObservableBoolean showProgress = new ObservableBoolean(true);
    public ObservableBoolean showContent = new ObservableBoolean();
    public ObservableBoolean showEmpty = new ObservableBoolean();
    public ObservableBoolean showDisplayAll = new ObservableBoolean(true);

    @Inject
    protected ReviewsRepository repository;
    protected ReviewsListView view;

    public ReviewsListViewModel(Context context, ReviewsListView view) {
        this.view = view;
        ReviewsApp.getApplication(context)
                .getComponent().inject(this);
    }

    public void loadReviews(boolean all) {

        Observable<List<Review>> observable;
        if (all) {
            observable = repository.getAllReviews();
            showDisplayAll.set(false);
        } else {
            observable = repository.getReviews();
            showDisplayAll.set(true);
        }

        observable
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(reviews -> {
                            showProgress.set(false);
                            if (reviews.isEmpty()) {
                                showEmpty.set(true);
                                showContent.set(false);
                            } else {
                                showEmpty.set(false);
                                showContent.set(true);
                            }
                            view.displayReviews(reviews);
                        },
                        throwable -> {

                            Log.e(LOG_TAG, "error", throwable);

                            showProgress.set(false);
                            showContent.set(true);
                            view.displayError("Error loading reviews");
                        }
                );
    }

    public void onClickShowAll(View view) {
        loadReviews(true);
    }
}
