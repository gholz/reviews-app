package com.guilhermeholz.reviews.viewmodel;

import android.databinding.BaseObservable;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.text.Html;
import android.text.Spanned;

import com.guilhermeholz.reviews.model.Review;

/**
 * Created by gholz on 6/2/16.
 */
public class ReviewItemViewModel extends BaseObservable {

    public ObservableField<String> title = new ObservableField<>();
    public ObservableField<String> message = new ObservableField<>();
    public ObservableField<Spanned> author = new ObservableField<>();
    public ObservableField<String> date = new ObservableField<>();
    public ObservableInt rating = new ObservableInt();

    public void display(Review review) {
        title.set(String.format("\"%s\"", review.title));
        message.set(review.message);
        author.set(Html.fromHtml("Reviewed by <b>" + review.author + "</b>"));
        date.set(review.date);
        rating.set((int) (review.rating * 100 / 5));
    }
}
