package com.guilhermeholz.reviews.viewmodel;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.util.Log;
import android.view.View;
import android.widget.RatingBar;

import com.guilhermeholz.reviews.ReviewsApp;
import com.guilhermeholz.reviews.domain.ReviewsRepository;
import com.guilhermeholz.reviews.model.Review;
import com.guilhermeholz.reviews.view.AddReviewView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.inject.Inject;

/**
 * Created by gholz on 6/2/16.
 */
public class AddReviewViewModel extends BaseObservable {

    private static final SimpleDateFormat OUTPUT_DATE_FORMAT = new SimpleDateFormat("MMMM dd, yyyy", Locale.US);
    private static final String LOG_TAG = AddReviewViewModel.class.getSimpleName();

    public ObservableField<String> title = new ObservableField<>("");
    public ObservableField<String> message = new ObservableField<>("");
    public ObservableField<String> author = new ObservableField<>("");
    public ObservableField<String> location = new ObservableField<>("");
    public ObservableInt rating = new ObservableInt(5);

    public ObservableBoolean displayNoTitleError = new ObservableBoolean();
    public ObservableBoolean displayNoMessageError = new ObservableBoolean();

    @Inject
    protected ReviewsRepository repository;
    private AddReviewView view;

    public AddReviewViewModel(Context context, AddReviewView view) {
        this.view = view;
        ReviewsApp.getApplication(context).getComponent().inject(this);
    }

    public void onClickAdd(View view) {

        displayNoTitleError.set(false);
        displayNoMessageError.set(false);

        if (title.get().isEmpty()) {
            displayNoTitleError.set(true);
        } else if (message.get().isEmpty()) {
            displayNoMessageError.set(true);
        } else {
            addReview();
        }
    }

    public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
        if (fromUser) {
            this.rating.set((int) rating);
        }
    }

    private void addReview() {

        String name = author.get().isEmpty() ? "a GetYourGuide Customer" : author.get();

        Review review = new Review();
        review.title = title.get();
        review.message = message.get();
        review.author = location.get().isEmpty() ? name : name + " - " + location.get();
        review.date = OUTPUT_DATE_FORMAT.format(new Date());
        review.rating = rating.get();

        repository.addReview(review)
                .subscribe(response -> {
                    if (response.success) {
                        view.onSuccess();
                    } else {
                        view.displayError("We couldn't add the review");
                    }
                }, throwable -> {
                    view.displayError("We couldn't add the review");
                    Log.e(LOG_TAG, "error", throwable);
                });
    }
}
