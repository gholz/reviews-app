package com.guilhermeholz.reviews;

import android.app.Application;
import android.content.Context;

import com.guilhermeholz.reviews.di.components.AppComponent;
import com.guilhermeholz.reviews.di.components.DaggerAppComponent;
import com.guilhermeholz.reviews.di.modules.AppModule;
import com.guilhermeholz.reviews.di.modules.NetworkModule;

/**
 * The main application provides a component
 * for dependency injection accessible from everywhere.
 */
public class ReviewsApp extends Application {

    private AppComponent component;

    @Override
    public void onCreate() {

        super.onCreate();

        component = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .networkModule(new NetworkModule())
                .build();
    }

    public static ReviewsApp getApplication(Context context) {
        return (ReviewsApp) context.getApplicationContext();
    }

    public AppComponent getComponent() {
        return component;
    }
}
