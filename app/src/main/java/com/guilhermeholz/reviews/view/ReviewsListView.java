package com.guilhermeholz.reviews.view;

import com.guilhermeholz.reviews.model.Review;

import java.util.List;

/**
 * Created by gholz on 6/2/16.
 */
public interface ReviewsListView {
    void displayReviews(List<Review> reviews);
    void displayError(String message);
}
