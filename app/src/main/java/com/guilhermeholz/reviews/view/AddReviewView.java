package com.guilhermeholz.reviews.view;

/**
 * Created by gholz on 6/3/16.
 */
public interface AddReviewView {
    void onSuccess();
    void displayError(String message);
}
